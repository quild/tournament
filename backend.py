#! env/bin/python3
"""Game backend runner."""
import tornado.web
from tornado.ioloop import IOLoop

from handlers.core import CleanHandler
from handlers.player import PlayerHandler
from handlers.tournament import Battlehandler, TournamentHandler


def make_app():
    return tornado.web.Application([
        (f'/player/new/', PlayerHandler),
        (f'/player/(?P<player_id>.*)/', PlayerHandler),
        (f'/players/', PlayerHandler),
        (f'/opponent/(?P<player_id>.*)/', Battlehandler),
        (f'/attack/', Battlehandler),
        (f'/tournament/', TournamentHandler),
        (f'/clean/', CleanHandler)
    ])


if __name__ == '__main__':
    app = make_app()
    app.listen(9999)
    IOLoop.current().start()
