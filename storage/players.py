"""Players database."""
import random
import time
from collections import OrderedDict
from uuid import uuid4


class Players(object):
    """Players class storage."""

    players = dict()

    def create(self, name):
        """Create new player."""
        player_id = str(uuid4())
        self.players[player_id] = {
            'name': name,
            'power': random.randint(0, 1000),
            'medals': 1000,
            'money': 0,
            'group': None,
            'battles': [],
            'last_attack': None,
        }

    def get(self, player_id):
        """Get player data by id."""
        try:
            result = self.players[player_id]
        except KeyError:
            result = {}
        return result

    def save_attack(self, from_player_id, to_player_id, atk_result):
        """Update attack player data."""
        player = self.players[from_player_id]
        opponent = self.players[to_player_id]
        player['battles'].append(to_player_id)
        player['medals'] += atk_result
        player['last_attack'] = time.time()
        opponent['medals'] -= atk_result

    def add_money(self, player_id, money):
        """Add money to player."""
        player = self.get(player_id)
        player['money'] += money

    def all(self, sort_by='power'):
        """Get all players from storage."""
        result = OrderedDict(sorted(self.players.items(),
                             key=lambda x: x[1][sort_by], reverse=True))
        return result

    def set_player_group(self, player_id, group_name):
        """Set group name to player after start tournament."""
        player = self.get(player_id)
        player['group'] = group_name

    def clean(self):
        """Clean players storage."""
        self.players = {}


# Init players DB
db = Players()
