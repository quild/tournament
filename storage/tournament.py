"""Tournamens storage class."""
import random
import time
from collections import defaultdict

from storage import players


class Tournament(object):
    """Tournamens storage."""

    start_time = None
    groups = defaultdict(set)
    busy_players = set()
    tournament_result = None
    TOURNAMET_TIME = 60 * 2  # 2 minutes

    def generate_group(self):
        """Generate group by power."""
        # clear data
        self.groups = defaultdict(set)

        users = list(players.db.all())
        group_counter = 1
        # Generate group by 50 players
        while users:
            group_name = f'group_{group_counter}'
            self.groups[group_name] = users[:50]
            # Save group name to players data
            for user in users[:50]:
                players.db.set_player_group(user, group_name)
            users = users[50:]
            group_counter += 1
        self.start_time = time.time()
        return self.groups

    def get_opponent(self, player_id):
        """Get player opponent for battle."""
        try:
            opponent_id = random.choice(self._avaiilable_players(player_id))
        except IndexError:
            # If opponent not fund
            return None
        self._fix_opponent_choise(player_id, opponent_id)
        return opponent_id

    def attack_opponent(self, from_player_id, to_player_id):
        """Attack player's opponent."""
        atk_result = random.randint(-10, 10)
        players.db.save_attack(from_player_id, to_player_id, atk_result)
        self._unfix_opponent_choise(from_player_id, to_player_id)

    def status(self):
        """Tournament status."""
        # Tournament turn is 2 minutes
        time_left = self.start_time + self.TOURNAMET_TIME - time.time()
        return 'run' if time_left > 0 else 'finish'

    def finish(self):
        """Finish tournament and calculation of results."""
        if not self.tournament_result:
            prepare_result = self._generate_result()
            self._reward_players_by_group(prepare_result)
            result = self._generate_result()
            self.tournament_result = result

    def show_result(self):
        """Show tournament result if it finish."""
        if self.status() == 'finish':
            self.finish()
        return self.tournament_result

    def clean(self):
        """Clean players storage."""
        self.start_time = None
        self.groups = defaultdict(set)
        self.busy_players = set()
        self.tournament_result = None

    def _avaiilable_players(self, player_id):
        """Find available to battle players."""
        player = players.db.get(player_id)
        return list(
            set(self.groups[player['group']]) -
            set(self.busy_players) -
            set(player['battles']) -
            set([player_id])
        )

    def _avaiilable_attack(self, player_id):
        """Check available attack."""
        # Check atatck per period
        player = players.db.get(player_id)
        last_attack = player.get('last_attack')
        if last_attack and time.time() - last_attack < 5:
            print('FALSE')
            return False
        else:
            return True
    
    def _reward_players_by_group(self, groups):
        """Rewarding players by group."""
        for group, group_players in groups.items():
            players.db.add_money(group_players[0]['id'], 300)
            players.db.add_money(group_players[1]['id'], 200)
            players.db.add_money(group_players[2]['id'], 100)

    def _generate_result(self):
        """Generate and format tournament result."""
        result = defaultdict(list)
        for pid, player in players.db.all('medals').items():
            result[player['group']].append({
                'id': pid,
                'name': player['name'],
                'power': player['power'],
                'medals': player['medals'],
                'money': player['money'],
                'battles': len(player['battles']),
            })
        return result

    def _fix_opponent_choise(self, from_player_id, to_player_id):
        """Fix attack choise into storages."""
        player = players.db.get(from_player_id)
        self.busy_players.add(to_player_id)
        self.groups[player['group']].remove(to_player_id)

    def _unfix_opponent_choise(self, from_player_id, to_player_id):
        """Unfix attack choise and return opponent to available list."""
        player = players.db.get(from_player_id)
        self.groups[player['group']].append(to_player_id)
        self.busy_players.remove(to_player_id)


db = Tournament()
