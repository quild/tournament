"""Tournament handlers for tornnado web server."""
from tornado.web import RequestHandler
from tornado.escape import json_decode

from storage import tournament as trn


class TournamentHandler(RequestHandler):
    """Tournament handler."""

    def get(self):
        """Show tournament status and result."""
        tournament_status = trn.db.status()
        result = {'status': tournament_status}
        if tournament_status == 'finish':
            result = {'result': trn.db.show_result()}
        self.write(result)

    def post(self):
        """Init tournament."""
        result = trn.db.generate_group()
        self.set_status(201)
        self.write(result)


class Battlehandler(RequestHandler):
    """Battle command handler."""

    def get(self, player_id):
        """Get opponent for battle."""
        if trn.db._avaiilable_attack(player_id):
            opponent_id = trn.db.get_opponent(player_id)
            if opponent_id:
                result = {'opponent_id': opponent_id}
            else:
                self.set_status(404)
                result = {'message': 'Available opponents not found'}
        else:
            self.set_status(402)
            result = {'message': 'To many attack per 5 seconds'}
        self.write(result)

    def post(self):
        """Attack opponent."""
        attack_data = json_decode(self.request.body)
        from_player_id = attack_data['from_player_id']
        to_player_id = attack_data['to_player_id']
        trn.db.attack_opponent(from_player_id, to_player_id)
        result = {'status': attack_data}
        self.write(result)