"""Player handlers for tornnado web server."""
from tornado.web import RequestHandler

from storage import players, tournament


class CleanHandler(RequestHandler):
    """Storage cleaner."""

    def post(self):
        """Clean all storage."""
        players.db.clean()
        tournament.db.clean()
        self.set_status(205)
        self.write({})
