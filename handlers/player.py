"""Player handlers for tornnado web server."""
from tornado.web import RequestHandler
from tornado.escape import json_decode

from storage import players


class PlayerHandler(RequestHandler):
    """Player handler."""

    def get(self, player_id=None):
        """Get player info by id."""
        if player_id:
            result = players.db.get(player_id)
            if not result:
                self.set_status(404)
                result = {
                    'message': 'Sorry, player not found'
                }
        else:
            result = players.db.all()
        self.write(result)

    def post(self):
        """Create new player."""
        data = json_decode(self.request.body)
        players.db.create(data['name'])
        result = {
            'status': 'created',
        }
        self.write(result)
