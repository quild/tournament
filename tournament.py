#! env/bin/python3
"""Tournament bot."""
import asyncio

from aiohttp import ClientSession

# Config
SERVER_URL = 'http://localhost:9999'
MAX_PLAYERS = 200


async def fetch_get(session, path):
    """Async get method."""
    async with session.get(f'{SERVER_URL}/{path}') as response:
        return await response.json()


async def fetch_post(session, path, data=None):
    """Async post method."""
    data = data or {}
    async with session.post(f'{SERVER_URL}/{path}', json=data) as response:
        return await response.json()


async def clean_storage():
    """Send clean storages request."""
    async with ClientSession() as session:
        await fetch_post(session, 'clean/')


async def create_user(name):
    """Send create user request to server."""
    async with ClientSession() as session:
        data = {'name': name}
        await fetch_post(session, 'player/new/', data)


async def create_users():
    """Create users corutine."""
    tasks = [asyncio.ensure_future(
        create_user(f'Player_{i}')) for i in range(MAX_PLAYERS)]
    await asyncio.wait(tasks)


async def start_tournament():
    """Send start tournament request."""
    async with ClientSession() as session:
        await fetch_post(session, 'tournament/')


async def player_attack(player_id):
    """Search and attack opponent."""
    async with ClientSession() as session:
        # Search opponrnt
        opponent = await fetch_get(session, f"opponent/{player_id}/")
        if opponent.get('opponent_id'):
            attack_data = {
                'from_player_id': player_id,
                'to_player_id': opponent['opponent_id']
            }
            await fetch_post(session, f"attack/", attack_data)


async def start_battle(player_id):
    """Start player battle."""
    async with ClientSession() as session:
        tournament = await fetch_get(session, 'tournament/')
        while tournament.get('status') == 'run':
            # While tournament search and attack opponent
            await player_attack(player_id)
            # Client pause 5 before new attack
            await asyncio.sleep(5)
            # Check tournament status
            tournament = await fetch_get(session, 'tournament/')


async def init_battle():
    """Start simulate players activity."""
    async with ClientSession() as session:
        # Get players list
        players = await fetch_get(session, 'players/')
    tasks = [
        asyncio.ensure_future(start_battle(player_id))
        for player_id in players]
    await asyncio.wait(tasks)


async def get_tournament_result():
    """Get tournament result."""
    async with ClientSession() as session:
        result = await fetch_get(session, 'tournament/')
        for group, players in result['result'].items():
            # Print tournament result
            print(f'GROUP: {group}')
            print("{:^10}{:^15}{:^10}{:^7}{:^10}".format(
                'Medals', 'Name', 'Money', 'Power', 'Battles'))
            for player in players:
                print(f"{player['medals']:<10}{player['name']:<15}"
                      f"{player['money']:<10}{player['power']:<7}"
                      f"{player['battles']}")


if __name__ == '__main__':
    print('[x] Init tourmnament client')
    ioloop = asyncio.get_event_loop()
    print('[x] Clean all storages')
    ioloop.run_until_complete(clean_storage())
    print('[x] Generate tournament users')
    ioloop.run_until_complete(create_users())
    print('[x] Start tournament')
    ioloop.run_until_complete(start_tournament())
    print('[x] Start battle')
    ioloop.run_until_complete(init_battle())
    print('[x] Get tournament result')
    ioloop.run_until_complete(get_tournament_result())
    print('[x] Stop tournament client')